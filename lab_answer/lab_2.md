### 1. Apakah perbedaan antara JSON dan XML?

| JSON                                                                | XML                                                                  |
| ------------------------------------------------------------------- | -------------------------------------------------------------------- |
| Sebuah cara merepresentasikan objek (lebih berdasarkan javascript). | Sebuah markup language yang digunakan untuk merepresentasikan objek. |
| Tidak mensupport namespaces.                                        | Mendukung namespaces.                                                |
| Mudah untuk dibaca.                                                 | Susah untuk dibaca.                                                  |
| Lebih tidak aman.                                                   | Data akan lebih aman daripada JSON.                                  |
| Men-support array.                                                  | Tidak men-support array.                                             |
| Lebih populer dan lebih muda dari XML.                              | Tidak populer dan lebih tua dari JSON.                         |

### 2. Apakah perbedaan antara HTML dan XML?

Pada dasarnya kesamaan terbesar yang dimiliki oleh HTML dan XML hanyalah strukturnya yang berdasarkan markup language. Selain itu, HTML dan XML memiliki perbedaan yang sangat besar karena mereka dibuat untuk dua hal yang berbeda, yaitu HTML untuk penyajian data dan XML untuk pengiriman data. Baik tag dalam HTML dan XML pun berbeda. Membandingkan HTML dengan XML seperti membandingkan Java dan Javascript yang sudah dengan jelas diketahui perbedaannya.