from rest_framework import serializers
from lab_1.serializers import FriendSerializer
from .models import Note

class NoteSerializer(serializers.ModelSerializer):
  receiver = FriendSerializer()
  sender = FriendSerializer()

  class Meta:
    model = Note
    fields = ('id', 'title', 'message', 'receiver', 'sender', )