from lab_2.models import Note
from django.shortcuts import render
from django.http.response import HttpResponse
from .serializers import NoteSerializer
from django.core import serializers
from rest_framework.renderers import JSONRenderer

def index(request):
  response = {'notes': Note.objects.all().values(),}
  return render(request, 'lab2.html', response)

def xml(request):
  # serializer = NoteSerializer(Note.objects.all(), many=True)
  return HttpResponse(serializers.serialize('xml', Note.objects.all()), content_type='application/xml')

def json(request):
  serializer = NoteSerializer(Note.objects.all(), many=True)
  return HttpResponse(JSONRenderer().render(serializer.data), content_type='application/json')