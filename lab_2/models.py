from django.db import models
from django.db.models.fields.related import ForeignKey
from lab_1.models import Friend

# Create your models here.
class Note(models.Model):
  receiver = models.ForeignKey(Friend, on_delete=models.CASCADE, related_name='receiver')
  sender = models.ForeignKey(Friend, on_delete=models.CASCADE, related_name='sender')
  title = models.CharField(max_length=100)
  message = models.TextField()

  def __str__(self):
    return f"{self.receiver.name} - {self.title}"