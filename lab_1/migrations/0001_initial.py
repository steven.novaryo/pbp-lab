# Generated by Django 3.2.7 on 2021-09-14 17:27

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Friend',
            fields=[
                ('name', models.CharField(max_length=30)),
                ('npm', models.IntegerField(primary_key=True, serialize=False, unique=True)),
                ('date_of_birth', models.DateField()),
            ],
        ),
    ]
