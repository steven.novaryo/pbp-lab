from django.db import models


class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(primary_key=True, unique=True, max_length=10)
    date_of_birth = models.DateField()

    def __str__(self):
        return f'{self.name} - {self.npm}'