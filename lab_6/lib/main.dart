import 'package:flutter/material.dart';
import 'package:lab_6/provider/application_color.dart';
import 'package:lab_6/screens/home.dart';
import 'package:lab_6/screens/settings.dart';
import 'package:provider/provider.dart';

void main() => runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ApplicationColor())
      ],
      child: MaterialApp(
        initialRoute: '/home',
        routes: {
          '/home': (context) => const Home(),
          '/settings': (context) => const SettingScreen(),
        },
        theme: ThemeData(
          // Define the default brightness and colors.
          brightness: Brightness.dark,
          // primaryColor: Colors.lightBlue[800],

          // Define the default font family.
          fontFamily: 'Raleway',
        )
      ),
    )
);