import 'package:flutter/material.dart';

class ICovidSwitchTile extends StatelessWidget {
  const ICovidSwitchTile({ 
    Key? key, 
    required this.onChanged,
    required this.tileText,
    required this.tileTextColor,
    required this.tileIcon,
    required this.secondaryTileIcon,
    required this.backgroundColor,
    required this.value,
  }) : super(key: key);

  final Function(bool) onChanged;
  final Color? backgroundColor;
  final Color? tileTextColor;
  final String tileText;
  final Icon tileIcon;
  final Icon secondaryTileIcon;
  final bool value;

  @override
  Widget build(BuildContext context) {
    return SwitchListTile(
      title: Text(tileText, 
        style: TextStyle(color: tileTextColor)
      ),
      onChanged: onChanged,
      tileColor: backgroundColor,
      secondary: value ? tileIcon : secondaryTileIcon,
      value: value,
      inactiveTrackColor: Colors.grey[300],
    );
  }
}