import 'package:flutter/material.dart';
import 'package:lab_6/provider/application_color.dart';
import 'package:lab_6/widget/home/input.dart';
import 'package:provider/provider.dart';

class HomeWidget extends StatelessWidget {
  const HomeWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    var iCovidLoginCard = Consumer<ApplicationColor>(
        builder: (context, applicationColor, _) => Card(
          color: applicationColor.backgroundColor2,
          child: Form(
            child: Column(children: [
                const ICovidTextField(labelText: 'username'),
                const ICovidTextField(labelText: 'password', obscureText: true),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 5),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                    ),
                    onPressed: () => showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('Anda Gagal Login'),
                        content: const Text('Yay!!'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'Cancel'),
                            child: const Text('Cancel'),
                          ),
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'OK'),
                            child: const Text('OK'),
                          ),
                        ],
                      )), 
                    child: const ICovidText(text: "Login", fontSize: 30),
                  ),
                ),
              ]),
          ),
          ));

    return Consumer<ApplicationColor>(
        builder: (context, applicationColor, _) => Container(
            decoration: BoxDecoration(
                color: const Color(0x83AAE1FF),
                image: DecorationImage(
                  colorFilter: ColorFilter.mode(
                      applicationColor.isDarkMode
                          ? Colors.blueGrey.withOpacity(0.2)
                          : const Color(0x83AAE1FF).withOpacity(0.1),
                      BlendMode.modulate),
                  image: const AssetImage('assets/login.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              child: Center(
                child: IntrinsicHeight(
                  child: Expanded(
                    // flex: ,
                    child: iCovidLoginCard
                  ),
                ),
              ),
                    
      )
    );
  }
}
