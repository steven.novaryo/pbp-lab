import 'package:flutter/material.dart';
import 'package:lab_6/provider/application_color.dart';
import 'package:provider/provider.dart';

class ICovidTextField extends StatelessWidget {
  const ICovidTextField({ Key? key,
    required this.labelText,
    this.obscureText = false,
  }) : super(key: key);

  final String labelText;
  final bool obscureText;

  @override
  Widget build(BuildContext context) {
    return Consumer<ApplicationColor>(
      builder: (context, applicationColor, _) => Padding(
          padding: const EdgeInsets.fromLTRB(25, 25, 25, 0),
          child: TextField(
                obscureText: obscureText,
                style: TextStyle(color: applicationColor.textColor),
                decoration: InputDecoration(
                  fillColor: applicationColor.textColor,
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: applicationColor.finalTextColor, width: 2.0),
                    borderRadius: BorderRadius.circular(100),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: applicationColor.finalTextColor, width: 2.0),
                    borderRadius: BorderRadius.circular(100),
                  ),
                  labelText: labelText,
                  labelStyle: TextStyle(color: applicationColor.finalTextColor),
                ),
              ),
        )
    );
  }
}

