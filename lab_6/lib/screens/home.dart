import 'package:flutter/material.dart';
import 'package:lab_6/provider/application_color.dart';
import 'package:lab_6/widget/home/index.dart';
import 'package:provider/provider.dart';

class Home extends StatelessWidget {
  const Home({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ApplicationColor>(
      builder: (context, applicationColor, _) => 
        Scaffold(
          appBar: PreferredSize(
            preferredSize: const Size.fromHeight(50.0),
              child: AppBar(
                centerTitle: true,
                backgroundColor: applicationColor.color,
                title: Text(
                  "Login",
                  style: TextStyle(color: Colors.blue[50]),
                ),
                actions: [
                  IconButton(
                    onPressed: () async {
                      await Navigator.pushNamed(context, '/settings'); 
                    },
                    icon: Icon(Icons.settings, color: applicationColor.textColor)
                  )
                ],
              ),
            ),
          backgroundColor: applicationColor.backgroundColor,
          body: const SafeArea(
            child: HomeWidget(),
          ),
        ),
      );
  }
}