import 'package:flutter/material.dart';
import 'package:lab_6/provider/application_color.dart';
import 'package:provider/provider.dart';

class SettingScreen extends StatelessWidget {
  const SettingScreen({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ApplicationColor>(
        builder: (context, applicationColor, _) => 
          Scaffold(
            appBar: PreferredSize(
              preferredSize: const Size.fromHeight(50.0),
              child: AppBar(
                backgroundColor: applicationColor.color,
                title: Text(
                  "Settings",
                  style: TextStyle(color: Colors.blue[50]),
                ),
              ),
            ),
            backgroundColor: applicationColor.backgroundColor,
            body: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: const [
                DarkModeSwitch(),
              ],
            ),
          ),
    );
  }
}