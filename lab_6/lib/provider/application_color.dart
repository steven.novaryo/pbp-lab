import 'package:flutter/material.dart';
import 'package:lab_6/widget/settings/tile.dart';
import 'package:provider/provider.dart';

class ApplicationColor with ChangeNotifier {
  bool _isDarkMode = false;

  bool get isDarkMode => _isDarkMode;
  set isDarkMode(bool value) {
    _isDarkMode = value;
    notifyListeners();
  }

  Color get finalTextColor => (_isDarkMode) ? Colors.grey : Colors.black;
  Color? get color => (_isDarkMode) ? Colors.blueGrey[700] : Colors.lightBlue;
  Color? get backgroundColor => (_isDarkMode) ? Colors.blueGrey : Colors.white;
  Color? get backgroundColor2 => (_isDarkMode) ? Colors.blueGrey[700] : Colors.grey[300];
  Color? get textColor => (_isDarkMode) ? Colors.grey[300] : Colors.grey[800];
}


class DarkModeSwitch extends StatefulWidget {
  const DarkModeSwitch({ Key? key }) : super(key: key);

  @override
  _DarkModeSwitchState createState() => _DarkModeSwitchState();
}

class _DarkModeSwitchState extends State<DarkModeSwitch> {
  @override
  Widget build(BuildContext context) {
    return Consumer<ApplicationColor>(
      builder: (context, applicationColor, _) => ICovidSwitchTile(
        tileText: 'Dark Mode',
        tileTextColor: applicationColor.textColor,
        onChanged: (newValue) {
          applicationColor.isDarkMode = newValue;
        },
        backgroundColor: applicationColor.backgroundColor,
        tileIcon: const Icon(Icons.nightlight, color: Colors.amber),
        secondaryTileIcon: const Icon(Icons.wb_sunny, color: Colors.amber),
        value: Provider.of<ApplicationColor>(context)._isDarkMode,
      )
    );
  }
}

// Consumer<ApplicationColor> colorCostumer(widget) => Consumer<ApplicationColor>(
//         builder: (context, applicationColor, _) => widget);

class ICovidText extends StatelessWidget {
  const ICovidText({ Key? key, 
    required this.text,
    this.fontSize = 14, 
  }) : super(key: key);

  final String text;
  final double fontSize;

  @override
  Widget build(BuildContext context) {
    return Consumer<ApplicationColor>(
      builder: (context, applicationColor, _) => Text(
        text,
        style: TextStyle(color: applicationColor.textColor, fontSize: fontSize),
      )
    );
  }
}