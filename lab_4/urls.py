from django.urls import path

from lab_2.views import index
from .views import add_note, index, note_list

urlpatterns = [
  path('', index, name='index'),
  path('add', add_note, name='add note'),
  path('note-list', note_list, name='note list'),
]
