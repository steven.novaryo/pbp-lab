from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from lab_2.models import Note
from django.shortcuts import render

from lab_4.forms import NoteForm

def index(request):
  response = {'notes': Note.objects.all().values(),}
  return render(request, 'lab4_index.html', response)

@login_required(login_url='/admin/login/')
def add_note(request):
  prop = {}
  form = NoteForm(request.POST or None, request.FILES or None)

  if form.is_valid():
    form.save()
    return HttpResponseRedirect(f'/lab-4/')
  
  prop['form'] = form
  prop['notes'] = Note.objects.all().values()
  return render(request, 'lab4_form.html', prop)

def note_list(request):
  response = {'notes': Note.objects.all().values(),}
  return render(request, 'lab4_note_list.html', response)