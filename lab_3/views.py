from django.db.models import query
from django.http.response import HttpResponseRedirect
from lab_1.models import Friend
from lab_1.serializers import FriendSerializer
from lab_3.forms import FriendForm
from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from django.contrib.auth.decorators import login_required


def index(request):
  prop = {
    'friends': Friend.objects.all().values()
  }
  return render(request, 'lab3_index.html', prop)

@login_required(login_url='/admin/login/')
def view_friend(request, npm):
  friend_object = Friend.objects.get(npm=npm)
  prop = {'friend': friend_object}
  return render(request, 'lab3_view_friend.html', prop)

@login_required(login_url='/admin/login/')
def add_friend(request):
  prop = {}
  form = FriendForm(request.POST or None, request.FILES or None)

  if form.is_valid():
    form.save()
    return HttpResponseRedirect(f'/lab-3/friends/{form.data["npm"]}')
  
  prop['form'] = form
  prop['friends'] = Friend.objects.all().values()
  return render(request, 'lab3_form.html', prop)

# class FriendView(generics.ListAPIView):
#   queryset = Friend.objects.all()
#   serializer_class = FriendSerializer

#   def get(self, request, npm, *args, **kwargs):
#     contacts = Friend.objects.get(npm=npm)
#     serializer = FriendSerializer(contacts)
#     return Response(serializer.data)