from django.urls import path
# from .views import FriendView
from .views import add_friend, view_friend, index

urlpatterns = [
  path('friends/<str:npm>', view_friend, name='view friend'),
  path('add', add_friend, name='add friend'),
  path('', index, name='index'),
  # path('friends/<str:npm>', FriendView.as_view(), name='get friend'),
]
