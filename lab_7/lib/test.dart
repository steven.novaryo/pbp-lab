// import 'package:flutter/material.dart';
// import 'package:lab_7/services.dart';

// void main() {
//   runApp(const MyApp());
// }

// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Lab 7 Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: const TrackerPage(),
//     );
//   }
// }

// class TrackerPage extends StatefulWidget {
//   const TrackerPage({ Key? key }) : super(key: key);

//   @override
//   _TrackerPageState createState() => _TrackerPageState();
// }

// class _TrackerPageState extends State<TrackerPage> {
//   TrackerService trackerService = TrackerService();
  
//   int kasusPositif = 0;

//   Future<void> update() async {
//     await trackerService.getKasus();
//     print(trackerService.data);
//     setState(() { kasusPositif = trackerService.data['positif'] ?? -100; });
//   }

//   @override
//   void initState() {
//     super.initState();
//     update();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(),
//       body: SafeArea(
//         child: Column(
//           children: [
//             Text('Kasus positif hari ini: $kasusPositif'),
//           ]
//         ),
//       ),
//     );
//   }
// }

// import 'dart:convert';

// import 'package:http/http.dart';

// class TrackerService {
//   late Map data;

//   Future<void> getKasus() async {
//     try {
//       Response response = await get(Uri.parse(
//           'https://apicovid19indonesia-v2.vercel.app/api/indonesia'));
//       print(response.body);
//       data = jsonDecode(response.body);
//       print(data);
//     } catch (e) {
//       print(e);
//     }
//     data = {};
//   }
// }