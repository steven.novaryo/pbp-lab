import 'dart:convert';

import 'package:http/http.dart';

class TrackerService {
  late Map data;

  Future<void> getKasus() async {
    try {
      Response response = await get(Uri.parse(
          'https://apicovid19indonesia-v2.vercel.app/api/indonesia'));
      print(response.body);
      data = jsonDecode(response.body);
      print(data);
    } catch (e) {
      print(e);
    }
    data = {};
  }
}