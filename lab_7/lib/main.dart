import 'package:flutter/material.dart';
import 'package:lab_7/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 7 Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const FormPage(),
    );
  }
}

class FormPage extends StatefulWidget {
  const FormPage({ Key? key }) : super(key: key);

  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  TrackerService trackerService = TrackerService();
  
  final _formKey = GlobalKey<FormState>();

  Map model = {
    'username': null,
    'password': null,
  };

  bool isSubmitDisabled = false;

  Future<void> onSubmit() async {
    if (isSubmitDisabled) return;
    setState(() {isSubmitDisabled = true;});
    // TODO: send request to server to enter login
    setState(() {isSubmitDisabled = false;});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Container(
          constraints: const BoxConstraints(minWidth: 200, maxWidth: 500),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Form(
              key: _formKey,
              autovalidateMode: AutovalidateMode.always,
              onChanged: () {
                Form.of(primaryFocus!.context!)!.save();
              },
              child: Column(
                children: [
                  TextFormField(
                    decoration: const InputDecoration(
                      icon: Icon(Icons.person),
                      labelText: 'Username',
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text!';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      model['username'] = value;
                    },
                  ),

                  TextFormField(
                    decoration: const InputDecoration(
                      icon: Icon(Icons.vpn_key),
                      labelText: 'Password',
                    ), 
                    obscureText: true,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text!';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      model['password'] = value;
                    },
                  ),

                  TextFormField(
                    decoration: const InputDecoration(
                      icon: Icon(Icons.vpn_key),
                      labelText: 'Repeat Password',
                      hintText: 'Please repeat your password',
                    ), 
                    obscureText: true,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter some text!';
                      }
                      if (value != model['password']) {
                        return 'Password doesn\'t match!';
                      }
                      return null;
                    },
                  ),

                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          margin: const EdgeInsets.only(top: 15.0, bottom: 15.0), 
                          child: const Divider(
                            color: Colors.black, 
                            height: 36
                          ),
                        ),
                      )
                    ],
                  ),

                  ElevatedButton(
                    onPressed: onSubmit,
                    style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 5),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                      ),
                    child: const Text('Submit')
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}